# coding: utf-8

import time
import re

from flask import Module, session, flash, redirect, render_template, request, g, jsonify, url_for
from functools import wraps
try:
	from flaskext import sqlalchemy
except ImportError:
	import flask_sqlalchemy as sqlalchemy
from wtforms.validators import ValidationError

from libora import app, db, product_preview_set
from models import *
from forms import *

bp = Module(__name__)

ADMIN_IPS = (
	'77.39.91.124',
	'127.0.0.1',
)

def camelcase2underscore(s):
    return re.sub('(?<=[a-z])([A-Z])', '_\\1', s).lower()

def returns_json(f):
	@wraps(f)
	def decorated(*args, **kwargs):
		res = f(*args, **kwargs)
		if isinstance(res, dict):
			return jsonify(res)
		return res
	return decorated

def login_required(f):
	@wraps(f)
	def decorated(*args, **kwargs):
		if not logged():
			flash(u'Вы должны авторизоваться!', 'error')
			return redirect('/admin/login')
		return f(*args, **kwargs)
	return decorated

def logged():
	return request.remote_addr in ADMIN_IPS or session.get('logged', False)

@bp.route('/')
@login_required
def index():
	from models import Product
	context = dict(

	)
	return render_template('admin/index.jinja', **context)

@bp.route('/Coupon', methods=['GET', 'POST'])
@login_required
def coupon():
	coupon = Coupon.query.first()
	if coupon is None:
		coupon = Coupon()
		db.session.add(coupon)

	if request.method == 'POST':
		code = request.form['code']
		discount = request.form['discount']
		coupon.code = code
		coupon.discount = discount
		db.session.commit()
		flash(u'Купон изменен')

	return render_template('admin/coupon.jinja', coupon=coupon)

@bp.route('/Review')
@login_required
def list_review():
	reviews = Review.query.order_by('-id').all()

	return render_template('admin/list_review.jinja', reviews=reviews, cls_name='Review')

@bp.route('/<cls_name>')
def list_record(cls_name):
	ModelClass = globals()[cls_name]
	record_name = camelcase2underscore(cls_name)

	records = ModelClass.query.order_by('-position' if hasattr(ModelClass, 'position') else '-id').all()
	return render_template('admin/list_{0}.jinja'.format(record_name), records=records,
						   record_name=record_name, cls_name=cls_name)

@bp.route('/Product/new', methods=['GET', 'POST'])
@login_required
def add_product():
	form = ProductForm(request.form)
	photos_form = ProductPhotosForm()

	is_xhr = request.is_xhr

	if request.method == 'POST' and form.validate():
		data = request.form.to_dict() if is_xhr else form.data
		if request.files['preview_filename'].filename:
			data['preview_filename']  = Product.__upload_set__.save(request.files['preview_filename'])

		product = Product(**data)
		db.session.add(product)
		db.session.commit()

		photos = request.files.getlist('photos')
		if len(photos):
			for photo_file in photos:
				if photo_file.filename:
					product_photo = ProductPhoto(product_id=product.id)
					product_photo.photo_filename = ProductPhoto.__upload_set__.save(photo_file)
					db.session.add(product_photo)
			db.session.commit()

		flash(u'Товар "%s" добавлена' % product.title)
		redirect_url = '/admin/Product/new' \
			if '_add_more' in request.form \
			else '/admin/Product'
		return redirect(redirect_url)

	context = dict(
		form=form,
		photos_form=photos_form,
		title=u'Добавление товара',
		add=True,
		cls_name='Product',
	)

	return render_template('admin/manage_record.jinja', **context)

@bp.route('/File/new', methods=['POST'])
@login_required
def add_file():
	files_count = 0

	for input_name in request.files:
		file = request.files[input_name]
		if not file.filename:
			continue

		files_count += 1

		m = re.match('file(\d+)?', input_name)
		i = m.groups()[0] or ''

		tags_string = request.form['tags' + i]
		filename = files_set.save(file)
		size = os.path.getsize(files_set.path(filename))
		file = File(filename=filename, description=request.form['description' + i],
					size=size, tags_string=tags_string)

		db.session.add(file)
		db.session.commit()
		
	if files_count:
		flash(u'Файл(ы) загружен(ы)')
	else:
		flash(u'Выберите файл', 'error')
	
	return redirect('/admin/File')

@bp.route('/<cls_name>/new', methods=['GET', 'POST'])
@login_required
def add_record(cls_name):

	FormClass = globals()['%sForm' % cls_name]
	ModelClass = globals()[cls_name]

	form = FormClass(request.form)

	if request.method == 'POST' and form.validate():
		data = form.data

		for input_name in request.files:
			file = request.files[input_name]
			if not file.filename:
				if input_name in data:
					del data[input_name]
				continue
			data[input_name] = ModelClass.__upload_set__.save(file)

		record = ModelClass(**data)

		db.session.add(record)
		db.session.commit()
		flash(u'Запись добавлена')

		if '_add_more' in request.form:
			redirect_url = '/admin/%s/new' % cls_name
		else:
			redirect_url = '/admin/%s' % cls_name
		return redirect(redirect_url)

	context = dict(
		form=form,
		add=True,
		cls_name=cls_name
	)

	return render_template('/admin/manage_record.jinja', **context)


@bp.route('/Product/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_product(id):
	is_xhr = request.is_xhr

	product = Product.query.get(id)

	form = ProductForm(request.form, product)
	photos_form = ProductPhotosForm()

	if request.method == 'POST' and (is_xhr or form.validate()):
		data = request.form.to_dict() if is_xhr else form.data

		if not is_xhr:
			if request.files['preview_filename'].filename:
				data['preview_filename']  = Product.__upload_set__.save(request.files['preview_filename'])
			else:
				del data['preview_filename']

		product.setattrs(**data)

		db.session.add(product)
		db.session.commit()

		photos = request.files.getlist('photos')
		if len(photos) and photos[0].filename:
			for photo_file in photos:
				product_photo = ProductPhoto(product_id=product.id)
				product_photo.photo_filename = ProductPhoto.__upload_set__.save(photo_file)
				db.session.add(product_photo)
			db.session.commit()

		if is_xhr:
			if request.args.get('type') == 'attr':
				return jsonify(dict(success=True))
			else:
				tags = Tag.query.all()
				return jsonify(
					success=True,
					file_html=render_template('admin/_{0}.jinja'.format(
						cls_name.lower()), file=record, cls_name=cls_name),
					tags_html=render_template('admin/tags_list.jinja', tags=tags),
				)
		else:
			flash(u'Товар "%s" изменена' % product.title)
			return redirect('/admin/Product#record{0}'.format(product.id))

	context = dict(
		form=form,
		photos_form=photos_form,
		record=product,
		photos=product.photos,
		title=u'Изменение работы',
		cls_name='Product',
		# photos=photos,
	)

	return render_template('admin/manage_record.jinja', **context)

@bp.route('/delete-photo', methods=['POST'])
def delete_photo():
	id = request.form['id']
	product_photo = ProductPhoto.query.get(id)
	db.session.delete(product_photo)
	db.session.commit()
	return jsonify(success=True)

@bp.route('/<cls_name>/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_record(cls_name, id):
	is_xhr = request.is_xhr
	
	ModelClass = globals()[cls_name]
	record = ModelClass.query.get(id)

	if not is_xhr:
		FormClass = globals()['%sForm' % cls_name]
		form = FormClass(request.form, obj=record)

	if request.method == 'POST' and (is_xhr or form.validate()):
		data = request.form.to_dict() if is_xhr else form.data

		for input_name in request.files:
			file = request.files[input_name]
			if not file.filename:
				if input_name in data:
					del data[input_name]
				continue
			data[input_name] = ModelClass.__upload_set__.save(file)
			
		record.setattrs(**data)

		db.session.add(record)
		db.session.commit()

		if is_xhr:
			if request.args.get('type') == 'attr':
				return jsonify(dict(success=True))
			else:
				tags = Tag.query.all()
				return jsonify(
					success=True,
					file_html=render_template('admin/_{0}.jinja'.format(
						cls_name.lower()), file=record, cls_name=cls_name),
					tags_html=render_template('admin/tags_list.jinja', tags=tags),
				)
		else:
			flash(u'Запись изменена')
			if '_add_more' in request.form:
				redirect_url = '/admin/%s/new' % cls_name
			else:
				redirect_url = '/admin/%s#record%s' % (cls_name, record.id)
			return redirect(redirect_url)

	context = dict(
		record=record,
		form=form,
		cls_name=cls_name,
	)

	if is_xhr:
		return jsonify(success=False)
	else:
		return render_template('/admin/manage_record.jinja', **context)

@bp.route('/Tag/')
@login_required
def tags_list():
	tags = Tag.query.all()

@bp.route('/Price/price/<int:id>', methods=['POST'])
@login_required
def edit_price_price(id):
	price = Price.query.get(id)
	price.price = request.form['value']

	db.session.add(price)
	db.session.commit()

	return 'ok'

@bp.route('/Price/min-pages/<int:id>', methods=['POST'])
@login_required
def edit_min_page_price(id):
	price = Price.query.get(id)
	price.min_pages = request.form['value']

	db.session.add(price)
	db.session.commit()

	return 'ok'

@bp.route('/Feedback/is-read/<int:id>', methods=['POST'])
@login_required
def edit_is_read_feedback(id):
	feedback = Feedback.query.get(id)
	feedback.is_read = not feedback.is_read

	db.session.add(feedback)
	db.session.commit()

	return 'ok'

@bp.route('/upload-work-photo/<int:id>', methods=['POST'])
@login_required
def upload_work_photo(id):
	work_photo = WorkPhoto(description=request.form['description'], work_id=id)
	work_photo.save_photo(request.files['photo'])

	db.session.add(work_photo)
	db.session.commit()

	return redirect(url_for('edit_work', id=id))

@bp.route('/File/delete/<int:id>', methods=['POST'])
@login_required
def delete_file(id):

	is_ajax = request.is_xhr

	if not id is None:
		try:
			record = File.query.get(id)
		except sqlalchemy.orm.exc.UnmappedInstanceError:
			record = None
			
		if record:
			if hasattr(record, 'before_destroy'):
				record.before_destroy()

			try:
				os.remove(record.path())
			except:
				pass

			db.session.delete(record)
			db.session.commit()

			if is_ajax:
				return 'ok'
			else:
				flash(u'Модель была удалена')
				return redirect('/admin/{0}'.format(cls_name))
	if is_ajax:
		return 'error'
	else:
		flash(u'Произошла ошибка', 'error')
		return redirect('/admin')

@bp.route('/<cls_name>/delete/<int:id>', methods=['POST'])
@login_required
def delete_record(cls_name, id):
	ModelClass = globals()[cls_name]

	is_ajax = request.is_xhr

	if not id is None:
		try:
			record = ModelClass.query.get(id)
		except sqlalchemy.orm.exc.UnmappedInstanceError:
			record = None
			
		if record:
			if hasattr(record, 'before_destroy'):
				record.before_destroy()
			db.session.delete(record)
			db.session.commit()

			if is_ajax:
				return 'ok'
			else:
				flash(u'Модель была удалена')
				return redirect('/admin/{0}'.format(cls_name))
	if is_ajax:
		return 'error'
	else:
		flash(u'Произошла ошибка', 'error')
		return redirect('/admin')

@bp.route('/<cls_name>/change-position', methods=['POST'])
@returns_json
@login_required
def change_record_position(cls_name):
	ids = request.form['ids'].split(',')
	ModelClass = globals()[cls_name]

	s = ''
	for position, id in enumerate(reversed(ids)):
		db.engine.execute(
			'UPDATE %s SET position = ? WHERE id = ?' % ModelClass.__tablename__, position, id)
	db.session.commit()

	return dict(result=True)

@bp.route('/<cls_name>/move/<int:id>')
@login_required
def move_record(cls_name, id):
	direction = request.args.get('direction', None)
	if direction is None:
		return 'error'

	ModelClass = globals()[cls_name]

	is_ajax = request.is_xhr

	if not id is None:
		try:
			record = ModelClass.query.get(id)
			if direction == 'up':
				next_record = ModelClass.query.order_by('position').filter(ModelClass.position > record.position).first()
			else:
				next_record = ModelClass.query.order_by('-position').filter(ModelClass.position < record.position).first()
		except sqlalchemy.orm.exc.UnmappedInstanceError:
			next_record = None
			
		if record and next_record:
			record.position, next_record.position = next_record.position, record.position
			db.session.add(record)
			db.session.add(next_record)
			db.session.commit()

			if is_ajax:
				return 'ok'
			else:
				flash(u'Порядок изменен')
				return redirect('/admin')
	if is_ajax:
		return 'error'
	else:
		flash(u'Произошла ошибка', 'error')
		return redirect('/admin')

@bp.route('/login', methods=['GET', 'POST'])
def login():
	form = LoginForm(request.form)
	status_code = 200

	if request.method == 'POST' and form.validate():
		if (request.form['username'] == app.config['ADMIN_USERNAME'] and
		request.form['password'] == app.config['ADMIN_PASSWORD']):
			flash(u'Вы усешно авторизовались.')
			session['logged'] = True
			return redirect('/admin')
		flash(u'Неправильные логин и/или пароль.', 'error')
		status_code = 401
	else:
		message = '<br>'.join(','.join(f.errors) for f in form if f.errors)
		if message:
			flash(message, 'error')

	body_class = 'class="login"'

	context = dict(
		form=form,
		body_class=body_class,
		not_logged=True,
	)

	return render_template('admin/login.jinja', **context), status_code

@bp.route('/admin/logout')
def logout():
	session['logged'] = False
	return redirect('/admin')