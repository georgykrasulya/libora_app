# encoding: utf-8

import cgi
import datetime
import json
import logging
import math
import os
import random
import shutil
import tempfile
from urllib2 import unquote

from flask import request, redirect, Blueprint, render_template, session, g, abort, make_response

from settings import ADMIN_EMAILS, MODE
from libora import (app, db, Message, mail)
from models.shop import *
from loggers import js_logger

MB = 1024 * 1024

bp = Blueprint('shop', __name__)

@bp.route('/log-js')
def log_js():
    error = u'{date}, {url}, {message} @ {filename}:{line}, {ua}\n'.format(**{
        'message': unquote(request.args.get('message', '')),
        'line': request.args.get('line'),
        'filename': unquote(request.args.get('filename')),
        'url': request.args.get('url'),
        'ua': request.args.get('ua'),
        'date': datetime.datetime.now(),
    })
    js_logger.error(error)
    return '', 204

@bp.before_request
def before_request():
    cart_product_ids = get_cart_product_ids()
    g.cart_product_ids = cart_product_ids
    g.cart_total_count = 0
    g.cart_total_sum = 0

    if cart_product_ids:
        g.cart_products = Product.query.filter(Product.id.in_(cart_product_ids)).all()
        g.cart_total_count = len(g.cart_products)
        g.cart_total_sum = get_products_sum(g.cart_products)

@bp.route('/')
def index():
    g.page = 'index'

    type_ = request.args.get('type_')
    action = request.args.get('action')

    products = Product.query.order_by(Product.position.desc())
    if type_:
        products = products.filter(Product.type_==Product.get_type_by_keyword(type_))
    if action:
        if action == 'sale':
            products = products.filter(Product.old_price!=0)
    products = products.all()

    return render_template('index.jinja',
        products=products,
        type_=type_,
        )

@bp.route('/product/<int:id>')
def product(id):
    g.page = 'product'
    product = Product.query.get(id)
    other_products = Product.query.order_by(db.func.RANDOM())[:3]
    g.page = 'product'
    return render_template('product.jinja',
        product=product, other_products=other_products
        )

@bp.route('/howto')
def howto():
    g.page = 'howto'
    return render_template('howto.jinja')

@bp.route('/delivery')
def delivery():
    g.page = 'delivery'
    return render_template('delivery.jinja')

@bp.route('/contacts')
def contacts():
    g.page = 'contacts'
    return render_template('contacts.jinja')

@bp.route('/reviews')
def reviews():
    g.page = 'reviews'
    reviews = Review.query.order_by('-id').all()
    return render_template('reviews.jinja',
        reviews=reviews,
        )

@bp.route('/cart')
def cart():
    g.page = 'cart'
    g.hide_page_cart = True

    products = Product.query.filter(Product.id.in_(g.cart_product_ids)).all()

    has_sale = len(filter(lambda product: product.old_price != 0, products))

    if len(products) == 0:
        return redirect('/')

    return render_template('cart.jinja',
        products=products, RING=Product.RING, EARRINGS=Product.EARRINGS,
        unicode=unicode,
        has_sale=has_sale,
        )

@bp.route('/check-coupon', methods=['POST'])
def check_coupon():
    code = request.form['coupon']
    coupon = Coupon.query.first()
    valid = True

    if coupon is None or not coupon.code:
        valid = False
    if coupon.code.lower() != code.lower():
        valid = False

    if valid:
        return json.dumps({
            'success': True,
            'discount': coupon.discount,
            })
    else:
        return json.dumps({
            'success': False,
            })

@bp.route('/order', methods=['POST'])
def order():
    ids = request.form.getlist('id')
    sizes = {}
    debug_info = []

    for id in ids:
        sizes[int(id)] = request.form.get('size{}'.format(id))

    products = Product.query.filter(Product.id.in_(ids))

    Product.query.filter(Product.id.in_(ids)).update({'postponed': True},
        synchronize_session=False,
        )
    db.session.commit()

    delivery = request.form['delivery']
    name = request.form['name']
    address = request.form['address']
    email = request.form['email']
    phone = request.form['phone']
    comment = request.form['comment']
    delivery_display = get_delivery_display(delivery)

    coupon_code = request.form.get('coupon', '')
    discount = 0
    coupon = Coupon.query.first()
    if coupon and coupon.code and coupon.code.lower() == coupon_code.lower():
        discount = coupon.discount

    debug_info.append('coupon_code: {}, coupon: {}, discount: {}'.format(coupon_code, coupon.discount, discount))

    order_total_sum = get_order_total_sum(products, delivery, discount)
    order_number = random.randint(1000, 9999)

    body = render_template('emails/new_order.eml',
        products=products, name=name, address=address, email=email,
        comment=comment, phone=phone, sizes=sizes,
        delivery_display=delivery_display,
        order_total_sum=order_total_sum,
        order_number=order_number,
        )

    msg = Message(u'Новый заказ #{}'.format(order_number),
        sender='noreply@libora.ru',
        recipients=ADMIN_EMAILS,
        body=body,
        )

    body_customer = render_template('emails/new_order_customer.eml',
        products=products, sizes=sizes, delivery_display=delivery_display,
        order_total_sum=order_total_sum,
        name=name,
        order_number=order_number,
        )

    msg_customer = Message(u'Заказ на libora.ru',
        sender='noreply@libora.ru',
        recipients=(email,),
        body=body_customer,
        )

    if MODE == 'PROD':
        mail.send(msg)
        mail.send(msg_customer)
    else:
        logging.debug(u'BODY:\n {}'.format(body))
        logging.debug(u'BODY CUSTOMER:\n {}'.format(body_customer))

    response = make_response(json.dumps({
        'success': True,
        'debug_info': debug_info
    }))
    response.set_cookie('cart_product_ids', '[]')
    return response

@bp.route('/order/thanks')
def thanks():
    return render_template('thanks.jinja',
        now=datetime.datetime.now(),
        )

@bp.route('/cart/add', methods=['POST'])
def cart_add():
    ids = [int(id) for id in request.form.getlist('ids')]

    new_products = Product.query.filter(Product.id.in_(ids)).all()
    if len(new_products) == 0:
        abort(404)

    cart_product_ids = g.cart_product_ids
    for id in ids:
        cart_product_ids.append(id)

    products = Product.query.filter(Product.id.in_(cart_product_ids)).all()
    cart_total_sum = get_products_sum(products)

    response_data = {
        'success': True,
        'cart_total_count': len(products),
        'cart_total_sum': cart_total_sum,
        'ids': ids,
        'html': [render_template('blocks/_page_cart_item.jinja', cart_product=product) for product in new_products]
    }
    response = make_response(json.dumps(response_data))
    response.set_cookie('cart_product_ids', json.dumps([product.id for product in products]))

    return response

@bp.route('/cart/delete', methods=['POST'])
def cart_delete():
    ids = [int(id) for id in request.form.getlist('ids')]

    cart_product_ids = g.cart_product_ids
    logging.info('ids to delete: {}, from: {}'.format(ids, cart_product_ids))
    for id in ids:
        if id in cart_product_ids:
            cart_product_ids.remove(id)

    products = Product.query.filter(Product.id.in_(cart_product_ids)).all()
    cart_total_sum = get_products_sum(products)

    response_data = {
        'success': True,
        'cart_total_count': len(products),
        'cart_total_sum': cart_total_sum,
        'ids': ids
    }
    response = make_response(json.dumps(response_data))
    response.set_cookie('cart_product_ids', json.dumps([product.id for product in products]))

    return response

def get_cart_product_ids():
    return list(set(json.loads(request.cookies.get('cart_product_ids', '[]'))))

def get_products_sum(products):
    cart_total_sum = 0
    for product in products:
        cart_total_sum += product.get_price_as_int()
    return cart_total_sum

def get_delivery_display(delivery):
    return {'post': u'Почта России', 'courier': u'Курьером в Москве или Санкт-Петербурге'}[delivery]

def get_delivery_price(delivery):
    if delivery == 'courier':
        return 350
    return 0

def get_order_total_sum(products, delivery, discount=0):
    sum_ = 0
    for product in products:
        if product.price:
            sum_ += product.get_price_as_int()

    if discount:
        sum_ = int(math.ceil(sum_ - (sum_ / 100.0 * discount)))

    sum_ += get_delivery_price(delivery)
    return sum_
