alter table products add column old_price integer not null default 0;
create index products_old_price on products (old_price);