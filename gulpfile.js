var gulp = require('gulp');
var less = require('gulp-less');
var rename = require('gulp-rename');
var cssmin = require('gulp-cssmin');
var include = require('gulp-include');
var concat = require('gulp-concat');
var myth = require('gulp-myth');
var Combine = require('stream-combiner');

var paths = {
    less: 'static/less/*',
    scripts: ['static/js/**/*.js']
}

gulp.task('less', function () {
    var combined = Combine(
        gulp.src('static/less/style.less'),
        less(),
        myth(),
        gulp.dest('static/css/')
    );

    combined.on('error', function(err) {
        console.error(err.message);
    });
});

gulp.task('minify', function () {
    gulp.src(['static/css/*', '!static/css/*.min.css'])
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('static/css/'));
});

gulp.task('scripts', function () {
    gulp.src('static/js/src/script.js')
        .pipe(include())
        .pipe(concat('build.js'))
        .pipe(gulp.dest('static/js/'));
});

gulp.task('default', ['less', 'minify']);

gulp.task('watch', function () {
    gulp.watch(paths.less, ['less', 'minify']),
    gulp.watch(paths.scripts, ['scripts']);
});