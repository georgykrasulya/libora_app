import datetime
import logging
import os

import settings

now = datetime.datetime.now()

fmt = '[%(asctime)s] [%(levelname)s in %(pathname)s:%(lineno)s %(message)s]'
formatter = logging.Formatter(fmt=fmt)

# payment_logging_level = logging.DEBUG if settings.MODE == 'DEV' else logging.INFO
# order_logging_level = logging.DEBUG if settings.MODE == 'DEV' else logging.INFO
js_logging_level = logging.DEBUG if settings.MODE == 'DEV' else logging.INFO

# payment_handler = logging.FileHandler(os.path.join(settings.LOG_DIR, 'payment', '{0}.log'.format(now.strftime('%Y-%m-%d'))))
# payment_handler.setFormatter(formatter)
# payment_logger = logging.getLogger('payment_logger')
# payment_logger.addHandler(payment_handler)
# payment_logger.setLevel(payment_logging_level)

# order_handler = logging.FileHandler(os.path.join(settings.LOG_DIR, 'order', '{0}.log'.format(now.strftime('%Y-%m-%d'))))
# order_handler.setFormatter(formatter)
# order_logger = logging.getLogger('order_logger')
# order_logger.addHandler(order_handler)
# order_logger.setLevel(order_logging_level)

js_handler = logging.FileHandler(os.path.join(settings.LOG_DIR, 'js', '{0}.log'.format(now.strftime('%Y-%m-%d'))))
js_handler.setFormatter(formatter)
js_logger = logging.getLogger('js_logger')
js_logger.addHandler(js_handler)
js_logger.setLevel(js_logging_level)