class ModelMixin(object):
    def setattrs(self, **kwargs):
        for kw, val in kwargs.iteritems():
            if hasattr(self, kw):
                setattr(self, kw, val)
        if hasattr(self, 'date_added') and not self.date_added and 'date_added' not in kwargs:
            self.date_added = datetime.datetime.now()
        if hasattr(self, 'position') and not self.position and 'position' not in kwargs:
            self.position = int(time())

def create_thumbnail(orig_path, path, size):
    img = Image.open(orig_path)
    img.thumbnail((size, size), Image.ANTIALIAS)
    img.save(path)
    
def camelcase2underscore(s):
    return re.sub('(?<=[a-z])([A-Z])', '_\\1', s).lower()