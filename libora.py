# coding: utf-8

import datetime
import random
import json
from urllib2 import unquote
from hashlib import md5

from flask import (Flask, render_template, redirect, request, flash, session, g, abort, session,
    make_response, Response)

import settings
from settings import MODE

try:
	from flaskext.mail import Mail, Message
except:
	from flask_mail import Mail, Message
try:
	from flaskext.sqlalchemy import SQLAlchemy, get_debug_queries
except:
	from flask_sqlalchemy import SQLAlchemy, get_debug_queries
try:
	from flaskext.uploads import UploadSet, IMAGES, configure_uploads
except:
	from flask_uploads import UploadSet, IMAGES, configure_uploads

from settings import rel, SITE_URL

app = Flask(__name__)
app.config.from_object('settings')

db = SQLAlchemy(app)
mail = Mail(app)

product_preview_set = UploadSet('productpreview', IMAGES)
product_photos_set = UploadSet('productphotos', IMAGES)
review_photo_set = UploadSet('reviewphoto', IMAGES)

configure_uploads(app, (product_preview_set, product_photos_set, review_photo_set))

@app.context_processor
def pass_context():
    return {'MODE': MODE, 'SITE_URL': SITE_URL, 'VERSION': settings.VERSION}

from views.shop import bp as shop_bp
from views.admin import bp as admin_bp

app.register_blueprint(shop_bp)
app.register_blueprint(admin_bp, url_prefix='/admin')
