import os
import datetime
import logging

DEBUG = True
MODE = 'PROD'

try:
	from local_settings import *
except ImportError:
	pass

ADMIN_USERNAME = 'admin'
ADMIN_PASSWORD = 'lesha'

VERSION = '0.3.17'

if MODE == 'DEV':
    PROJECT_DIR = os.path.abspath(os.path.dirname(__file__))
    SITE_URL = 'http://localhost:5000'
    STATIC_DIR = os.path.join(PROJECT_DIR, 'static')
    SQLALCHEMY_DATABASE_URI = 'sqlite:///libora.db'
    logging.getLogger().setLevel(logging.DEBUG)
else:
    PROJECT_DIR = os.path.join(os.path.expanduser('~'), 'flask-projects/libora')
    SITE_URL = 'http://libora.ru'
    STATIC_DIR = os.path.join(os.path.expanduser('~'), 'domains/libora.ru/static/')
    SQLALCHEMY_DATABASE_URI = 'sqlite:///%s' % os.path.join(PROJECT_DIR, 'libora_production.db')
    logging.getLogger().setLevel(logging.WARNING)

def rel(*x):
    return os.path.join(PROJECT_DIR, *x)

LOG_DIR = rel('logs')

SECRET_KEY = '\xa0\xc0S[\xa5w\xd5\xca\xd4\xa6*S\xe89"]ec\x0c\xb09cB\x1e\xa6\xfd\x01\xf4 \xe6\x16\xe5\x98?\x06{x\xaa\xf8\xb9\xdfD\x16'

UPLOADED_PRODUCTPREVIEW_DEST = os.path.join(STATIC_DIR, 'product/preview')
UPLOADED_PRODUCTPREVIEW_URL = '/static/product/preview/'

UPLOADED_PRODUCTPHOTOS_DEST = os.path.join(STATIC_DIR, 'product/photos')
UPLOADED_PRODUCTPHOTOS_URL = '/static/product/photos/'

UPLOADED_REVIEWPHOTO_DEST = os.path.join(STATIC_DIR, 'review/photo')
UPLOADED_REVIEWPHOTO_URL = '/static/review/photo/'

ADMIN_EMAILS = ('gkrasulya@gmail.com', 'elikpero@gmail.com', 'liborashop@gmail.com',)

MAIL_SERVER = 'localhost'
MAIL_PORT = 25
MAIL_USE_TLS = False
MAIL_USE_SSL = False
MAIL_DEBUG = False
MAIL_USERNAME = 'feedback@8hands.ru'
MAIL_PASSWORD = 'gosha123'