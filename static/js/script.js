/* Author: Georgy Krasulya

*/

(function() {

    $.ajaxSetup({
        dataType: 'json'
    });

    var $b = $('body');
    var $w = $(window);
    var $scroll = $('html, body');

    var $header = $('#page-header');
    var $content = $('#page-content');
    var $footer = $('#page-footer');


    LIBORA.scrollTo = function($elem, opts) {
        var opts = $.extend({
            offset: 0
        }, opts || {});

        $scroll.animate({
            scrollTop: $elem.offset().top + opts.offset
        }, 300);
    };


    var PageCart = {
        init: function() {
            this.$cart = $('#page-header-cart');
            this.$list = $('#page-header-cart-list');
            this.$title = this.$cart.find('.js-title');
            this.$totalCount = this.$cart.find('.js-total-count');
            this.$totalSum = this.$cart.find('.js-total-sum');

            $b.on('click', $.proxy(function(e) {
                this._hide();
            }, this));

            this.$cart.on('click', function(e) {
                e.stopPropagation();
            });

            this.$title.on('click', $.proxy(this._onCartTitleClick, this));

            this.$cart.on('click', '.js-delete', $.proxy(this._onDeleteClick, this));

            this.productAddedEvent = LIBORA.Event();
            this.productDeletedEvent = LIBORA.Event();

            this._bindScroll();
        },

        _hide: function() {
            this._toggle(false);
        },

        _show: function() {
            this._toggle(true);
        },

        _toggle: function(open) {
            this.$cart.toggleClass('page-header__cart_opened', open);
        },

        _bindScroll: function() {
            this.isPageHeaderFixed = false;

            $w.on('scroll', $.proxy(function() {
                var scrollTop = $w.scrollTop();
                if (scrollTop > 120 && ! this.isPageHeaderFixed) {
                    this.isPageHeaderFixed = true;
                    this.$cart.addClass('page-header__cart_fixed');
                }
                if (scrollTop < 120 && this.isPageHeaderFixed) {
                    this.isPageHeaderFixed = false;
                    this.$cart.removeClass('page-header__cart_fixed');
                }
            }, this));
        },

        _onDeleteClick: function(e) {
            e.preventDefault();

            var $link = $(e.currentTarget);
            var $item = $link.closest('.js-page-header-cart-dropdown-list-item');
            var id = $item.data('id');

            this._deleteFromCart(id);
            $item.hide();
        },

        _getItem: function(id) {
            return this.$list.find('.js-page-header-cart-dropdown-list-item[data-id=' + id + ']')
        },

        _onCartTitleClick: function(e) {
            e.preventDefault();
            e.stopPropagation();

            this._toggle();
        },

        _deleteFromCart: function(id) {
            $.ajax({
                url: '/cart/delete',
                type: 'POST',
                data: {
                    ids: id
                }
            })
            .done($.proxy(function(res) {
                if (res.success) {
                    this._productDeleted(res);
                } else {
                    console.log('not success');
                }
            }, this))
            .fail($.proxy(function(xhr, textStatus, error) {

            }, this));
        },

        deleteFromCart: function(id) {
            this._deleteFromCart(id);
            this._getItem(id).hide();
        },

        addToCart: function(id) {
            ga('send', 'event', 'Cart', 'Add', 'Add', id);

            $.ajax({
                url: '/cart/add',
                type: 'POST',
                data: {
                    ids: id
                }
            })
            .done($.proxy(function(res) {
                if (res.success) {
                    this._productAdded(res);
                } else {
                    // console.log('not success');
                }
            }, this))
            .fail($.proxy(function(xhr, textStatus, error) {
                // console.log('error');
            }, this));
        },

        _productDeleted: function(res) {
            this.updateCart(res);
            $.each(res.ids, $.proxy(function(i, id) {
                var $item = this._getItem(id);
                $item.remove();
                this.productDeletedEvent.fire(id);
            }, this));
        },

        _productAdded: function(res) {
            var $fragment = $($.parseHTML(res.html.join('\n')));
            this.$list.prepend($fragment);
            this.updateCart(res);

            this.$cart.addClass('page-header__cart_new-product');
            setTimeout($.proxy(function() {
                this.$cart.removeClass('page-header__cart_new-product');
            }, this), 500);
        },

        updateCart: function(res) {
            this.$totalCount.html(res.cart_total_count);
            this.$totalSum.html(res.cart_total_sum);
            this.$cart.toggleClass('page-header__cart_empty', ! res.cart_total_count);
        }
    }

    PageCart.init();
    LIBORA.PageCart = PageCart;

    var ProductList = {
        init: function() {

            this.$filterSelect = $('#product-filter-select');

            $b.on('click', '.js-product-item-buy-btn', $.proxy(this._onBuyClick, this));

            this.$filterSelect.on('change', $.proxy(this._onFilterSelectChange, this));

            this.$productItems = $('.js-product-item');
            this.$productItemsNotInStock = this.$productItems.filter('[data-in-stock=0]');

            LIBORA.PageCart.productDeletedEvent.on($.proxy(function(id) {
                this._getItem(id).removeClass('product-item_in-cart');
            }, this));

            this._onFilterSelectChange();
        },

        _onFilterSelectChange: function(e) {
            this.$productItemsNotInStock.toggleClass('product-item_hidden', this.$filterSelect[0].value === 'in_stock');
        },

        _onBuyClick: function(e) {
            e.preventDefault();

            $btn = $(e.currentTarget);
            $item = $btn.closest('.js-product-item');
            id = $item.data('id');
            $likePopup = $item.find('.js-product-item-like-popup');

            $likePopup.show().addClass('product-item__bottom__like-popup_opened');

            setTimeout($.proxy(function() {
                $likePopup.addClass('product-item__bottom__like-popup_away');
                setTimeout(function() {
                    $likePopup.hide();
                }, 500);
            }, this), 500);

            LIBORA.PageCart.addToCart(id);
            $item.addClass('product-item_in-cart');
        },

        _getItem: function(id) {
            return $('.js-product-item[data-id=' + id + ']');
        }
    };

    if ($('#product-filter-select').length) {
        ProductList.init();
    }
    LIBORA.ProductList = ProductList;

    $b.on('click', '.js-scroll-to-contacts', function(e) {
        e.preventDefault();

        $scroll.animate({
            scrollTop: $footer.offset().top
        }, 300);
    });



    var errorHandler = function(message, filename, line) {
        try {
            var error = {
                message: encodeURIComponent(message),
                filename: encodeURIComponent(filename),
                line: encodeURIComponent(line),
                ua: navigator.userAgent,
                url: document.location.href.toString()
            },
            param = $.param(error);
            new Image().src = '/log-js' + '?' + param;
            SHOP.log(message, 'in ' + filename + ' at line ' + line);
        } catch(e) {};
    };
    window.onerror = errorHandler;


})();
