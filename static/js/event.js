(function() {


    LIBORA.Event = function() {
        var event_ = {
            init: function() {
                this._handlers = [];
            },

            on: function(fn) {
                if (! (fn in this._handlers)) {
                    this._handlers.push(fn);
                }
            },

            un: function(fn) {
                if (fn in this._handlers) {
                    var index = this._handlers.indexOf(fn);
                    this._handlers.splice(index, 1);
                }   
            },

            fire: function() {
                var args = Array.prototype.slice.call(arguments);
                $.each(this._handlers, function(i, handler) {
                    handler.apply(null, args);
                });
            }
            
        }

        event_.init();

        return event_;
    }
})();