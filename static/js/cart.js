if (LIBORA.page === 'cart') {

    var $b = $('body');
    var $w = $(window);
    var $scroll = $('html, body');

    var PageCart = LIBORA.PageCart;

    var Cart = {
        init: function() {
            $b.on('click', '.js-cart-products-item-delete', $.proxy(this._onDeleteClick, this));

            this.deliveryPrices = {
                'post': 0,
                'courier': 350
            };

            this.discount = 0;

            this.$couponForm = $('#coupon-form');
            this.$couponError = $('.js-input-error', this.$couponForm);
            this.$couponBtn = $('.btn', this.$couponForm);
            this.$couponInput = $('#coupon');

            this.$form = $('#cart-form');
            this.$info = $('#cart-info');
            this.$infoButton = $('#cart-info-button');
            this.$infoInputs = $('.js-info-input');

            this.$totalCount = $('.js-cart-total-count');
            this.$totalSum = $('.js-cart-total-sum');
            this.$totalHint = $('.js-cart-total-hint');
            this.$orderTotalSum = $('.js-order-total-sum');
            this.$productList = $('#cart-product-list');

            this.$delivery = $('.js-delivery');

            this.$buttonStep2 = $('#button-step2');
            this.$buttonStep3 = $('#button-step3');

            this.$step2 = $('#step2');
            this.$step3 = $('#step3');
            this.$step4 = $('#step4');

            this.$submit = $('#submit');

            this.$delivery.on('change', $.proxy(this._onDeliveryChange, this));

            this.$buttonStep2.on('click', $.proxy(this._onButtonStep2Click, this));
            this.$buttonStep3.on('click', $.proxy(this._onButtonStep3Click, this));

            this._bindInputs();

            this.$form.on('submit', $.proxy(this._onSubmit, this));

            this.$couponInput.on('input', $.proxy(function(e) {
                this.$couponError.hide();
            }, this));

            this.$couponBtn.on('click', $.proxy(function(e) {
                e.preventDefault();

                this.$couponError.hide();

                var coupon = $.trim(this.$couponInput.val());

                if (coupon) {
                    this._checkCoupon(coupon);
                } else {
                    this._showCouponError('Введите купон');
                }
            }, this));

            this.$infoButton.on('click', $.proxy(function(e) {
                e.preventDefault();

                if (this._validate()) {
                    this.$step3.show();
                    LIBORA.scrollTo(this.$step3, {
                        offset: -50
                    });
                    ga('send', 'event', 'Cart', 'Finish Step 1', 'Finish Step 1');
                }
            }, this));

             $("#address").suggestions({
                serviceUrl: "https://dadata.ru/api/v1/suggest/address",
                selectOnSpace: true,
                token: "e3a1f44f49de7b910b6ae00a6d01f595a6a2525a",
                onSelect: function(suggestion) {
                    // console.log(suggestion);
                }
            });
        },

        _showCouponError: function(error) {
            error = error || 'Неправильный купон';
            this.$couponError.html(error).show();
        },

        _checkCoupon: function(coupon) {
            $.ajax({
                url: '/check-coupon',
                type: 'POST',
                data: {
                    coupon: coupon
                }
            })
            .done($.proxy(function(res) {
                if (res.success) {
                    this.coupon = coupon;
                    this._enableDiscount(res.discount);
                } else {
                    this._showCouponError();
                }
            }, this))
            .fail($.proxy(function() {

            }, this));
        },

        _enableDiscount: function(discount) {
            this.discount = discount;
            this._recount();
        },

        _bindInputs: function() {
            var $selects = this._getSizeSelects();
            $selects.each(function(i, select) {
                var $select = $(select);
                var $error = $select.closest('.js-input-wrap').find('.js-input-error');
                $select.data('$error', $error);
                $select.on('change', function() {
                    $error.hide();
                });
            });

            this.$infoInputs.each(function(i, input) {
                var $input = $(input);
                var $error = $input.parent().find('.js-input-error');
                $input.data('$error', $error);
                $input.on('input change focus', function() {
                    $error.hide();
                });
            });
        },

        _onSubmit: function(e) {
            e.preventDefault();

            if (this._validate()) {

                ga('send', 'event', 'Cart', 'Submit', 'Submit');

                this.$submit.attr('disabled', true);

                $.ajax({
                    data: this.$form.serialize(),
                    url: '/order',
                    type: 'POST'
                })
                .done($.proxy(function(res) {
                    if (res.success) {
                        this.$submit.hide();
                        document.location.href = '/order/thanks';
                    } else {

                    }
                }, this))
                .fail($.proxy(function(xhr, textStatus, error) {
                    
                }, this))
                .always($.proxy(function() {
                    this.$submit.attr('disabled', false);
                }, this));
                
            }

        },

        _getSizeSelects: function() {
            return $('.js-size');
        },

        _onButtonStep3Click: function(e) {

        },

        _onButtonStep2Click: function(e) {
            e.preventDefault();

            if (this._validateSize()) {
                this.$step2.show();
                LIBORA.scrollTo(this.$step2, {
                    offset: -50
                });
                ga('send', 'event', 'Cart', 'Finish Step 1', 'Finish Step 1');
            }
        },

        _validate: function() {
            valid = this._validateSize();
            if (valid) {
                this.$infoInputs.each($.proxy(function(i, input) {
                    var inputValid = true;
                    var $input = $(input);
                    var value = input.value
                    if ($input.attr('required') && value.replace(/^\s+|\s+$/) === '') {
                        inputValid = false;
                    } else if (input.name === 'phone' && ! this._validatePhone(value)) {
                        inputValid = false;
                    } else if (input.name === 'email' && ! this._validateEmail(value)) {
                        inputValid = false;
                    }
                    if (! inputValid) {
                        valid = false;
                        $input.data('$error').show();
                    }
                }, this));

                if (! valid) {
                    LIBORA.scrollTo(this.$info, {
                        offset: -50
                    });
                }
            }

            return valid;
        },

        _validatePhone: function(value) {
            return value.match(/\d{10}/);
        },

        _validateEmail: function(value) {
            return value.match(/.+@.+\..+/)
        },

        _validateSize: function() {
            var $selects = this._getSizeSelects();
            var valid = true;
            $selects.each(function(i, select) {
                if (select.value == '') {
                    $(select).data('$error').show();
                    valid = false;
                }
            });

            if (! valid) {
                LIBORA.scrollTo(this.$productList, {
                    offset: -50
                });
            }

            return valid;
        },

        _onDeleteClick: function(e) {
            e.preventDefault();

            var $link = $(e.currentTarget);
            var $item = $link.closest('.js-cart-products-item');
            var id = $item.data('id');

            this._deleteItem(id);

            PageCart.deleteFromCart(id);

            $item.fadeOut($.proxy(function() {
                $item.remove();
                this._recount();
                if (this._getProductsItems().length === 0) {
                    document.location.href = '/';
                }
            }, this));

        },

        _getProductsItems: function() {
            return $('.js-cart-products-item');
        },

        _onDeliveryChange: function(e) {
            this.$step4.show();
            ga('send', 'event', 'Cart', 'Finish Step 3', 'Finish Step 3');
            LIBORA.scrollTo(this.$step4, {
                offset: -50
            });
            this._recount();
        },

        _recount: function() {
            var $productsItems = this._getProductsItems();
            var totalCount = $productsItems.length;
            var totalSum = this._getTotalSum($productsItems);

            if (this.discount) {
                totalSum = Math.ceil(totalSum - (totalSum / 100 * this.discount))
                // orderTotalSum = Math.ceil(orderTotalSum - (orderTotalSum / 100 * 10))

                this.$totalHint.html('С учетом скидки ' + this.discount + '%');
            }

            var orderTotalSum = totalSum + this._getDeliveryPrice();

            this.$totalCount.html(totalCount);
            this.$totalSum.html(totalSum);
            this.$orderTotalSum.html(orderTotalSum);
        },

        _getTotalSum: function($productsItems) {
            var totalSum = 0;
            $productsItems.each(function(i, item) {
                totalSum += +$(item).data('price');
            });
            return totalSum;
        },

        _getDeliveryValue: function() {
            return this.$delivery.filter(':checked').val();
        },

        _getDeliveryPrice: function() {
            return this.deliveryPrices[this._getDeliveryValue()] || 0;
        },

        _deleteItem: function(id) {

        }
    };

    Cart.init();

    LIBORA.Cart = Cart;


}
