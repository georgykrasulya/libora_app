(function() {

    if (LIBORA.page === 'product') {

        var data = LIBORA.data;
        var $w = $(window);
        var $b = $('body');

        var Product = {
            init: function() {
                this.likeClassnameOpened = 'product__content__actions__like-popup_opened';
                this.likeClassnameAway = 'product__content__actions__like-popup_away';

                this.id = data.currentProductId;
                this.$wrap = $('#product');
                this.$likePopup = $('#like-popup');
                this.$buyButton = $('#buy-button');

                this.$content = $('#product-content');
                this.contentClassnameFixed = this.$content.data('classname-fixed');
                this.contentHeight = this.$content.outerHeight();
                this.contentOffsetTop = this.$content.offset().top;
                this.isContentFixed = false;

                this.$buyButton.on('click', $.proxy(this._onBuyClick, this));
                LIBORA.PageCart.productDeletedEvent.on($.proxy(this._onPageCartProductDeleted, this));

                this.productBottom = this._getProductBottom();

                $w.on('load', $.proxy(function() {
                    this.productBottom = this._getProductBottom();
                }, this));

                $w.on('scroll', $.proxy(this._onScroll, this));
            },

            _getProductBottom: function() {
                return this.$wrap.offset().top + this.$wrap.outerHeight();
            },

            _onScroll: function() {
                var scrollTop = $w.scrollTop();

                if (scrollTop + 50 > this.contentOffsetTop && ! this.isContentFixed) {
                    this.$content.addClass(this.contentClassnameFixed);
                    this.isContentFixed = true;
                    this.$content.css('margin-top', 0);
                } else if (scrollTop + 50 < this.contentOffsetTop && this.isContentFixed) {
                    this.$content.removeClass(this.contentClassnameFixed);
                    this.isContentFixed = false;
                }

                if (this.isContentFixed) {
                    var edge = scrollTop + this.contentHeight + 100;
                    if (edge > this.productBottom) {
                        this.$content.css('margin-top', -(edge - this.productBottom));
                    } else {
                        this.$content.css('margin-top', 0);
                    }
                }
            },

            _onPageCartProductDeleted: function(id) {
                if (this.id == id) {
                    this.$likePopup.removeClass(this.likeClassnameOpened + ' ' + this.likeClassnameAway);
                    this.$wrap.removeClass('product_in-cart');
                }
            },

            _onBuyClick: function(e) {
                e.preventDefault();

                this._showLike();
                this.$wrap.addClass('product_in-cart');
                LIBORA.PageCart.addToCart(this.id);
            },

            _showLike: function() {
                this.$likePopup.addClass(this.likeClassnameOpened);

                setTimeout($.proxy(function() {
                    this.$likePopup.removeClass(this.likeClassnameOpened);
                    this.$likePopup.addClass(this.likeClassnameAway);
                }, this), 500);
            }
        };

        Product.init();

    }

})();