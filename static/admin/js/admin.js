jQuery(function($) {
	var $w = $(window),
		$al = $('#ajaxLoading')

	$al.ajaxSend(function() {
		$al.css('opacity', 1).removeClass('error').removeClass('ok').html('Сохраняется...').fadeIn('fast');
	});

	$al.data('init-top', $al.position().top);
	$w.scroll(function() {
		if ($w.scrollTop() > $al.data('init-top') - 50) {
			$al.css('margin-top', $w.scrollTop() - $al.data('init-top') + 50);
		} else {
			$al.css('margin-top', 25);
		}
	});

	$('#ajaxLoading').ajaxSuccess(function() {
		$al.removeClass('error').addClass('ok').html('Сохранено');
		setTimeout(function() {
			$al.fadeOut();
		}, 2000);
	});

	$('#ajaxLoading').ajaxError(function() {
		$al.removeClass('ok').addClass('error').html('Произошла ошибка');
		setTimeout(function() {
			$al.fadeOut();
		}, 2000);
	});

	$('.js-hide').hide();

	var $b = $(document.body);

	$b.on('click', '.summary', function() {
		var $details = $(this).parents('.details:eq(0)');

		window.q = $details;

		console.log($details);

		if ($details.hasClass('opened')) {
			$details.removeClass('opened');
		} else {
			$details.addClass('opened');
		}

		return false;
	});

	$b.on('click', '.close-details', function() {
		var $details = $(this).parents('.details:eq(0)');

		$details.removeClass('opened');

		return false;
	});

	var $editableList = $('.editable-list');

	if ($editableList.length) {

		var $rows = $editableList.find('tbody tr'),
			formSubmitHandler = function() {
				var $form = $(this);

				console.log(this);
				console.log(this.action);

				$.post(this.action, $form.serialize(), function(res) {
					if (res && res.success) {
						var $row = $form.parents('tr:eq(0)');
					
						$row.html(res.file_html);
						$row.find('form').submit(formSubmitHandler);

						$('#tagList').html(res.tags_html);
					}
				}, 'json');

				return false;
			}

		$rows.each(function(i, row) {
			var $row = $(row);

			$row.on('click', 'a.edit-row-link', function() {
				$row.find('form').show();
				$row.find('.editable-item').hide();

				return false;
			});

			$row.on('click', 'a.cancel', function() {
				$row.find('form').hide();
				$row.find('.editable-item').show();

				return false;
			});

			$row.find('form').submit(formSubmitHandler);
		});
	}

	$('.tooltip, .actions a, .inline-actions a, .tip').tooltip();

	var $feedbackList = $('#feedbackList');

	if ($feedbackList.length) {
		var $feedbackTitles = $feedbackList.find('.message'),
			$changeIsRead = $feedbackList.find('a.change-is-read');

		$feedbackTitles.each(function(i, el) {
			$(el).click(function() {
				$(el).toggleClass('opened');
				return false;
			});
		});

		$changeIsRead.each(function() {
			var $self = $(this),
				$row = $self.parents('tr:eq(0)'),
				$isRead = $row.find('.is-read');

			$self.click(function() {
				$.post($self[0].href, function(res) {
					if ($isRead.hasClass('icon-plus')) {
						$isRead.addClass('icon-minus');
						$isRead.removeClass('icon-plus');
					} else {
						$isRead.addClass('icon-plus');
						$isRead.removeClass('icon-minus');
					}
				});

				return false;
			});
		});

		var $editInputs = $('table.admin-list input.input-price, table.admin-list input.input-min-pages');

		$editInputs.each(function(i, el) {
			var $editId = el.id.match(/(\d+)/)[1],
				property = $(el).hasClass('input-price') ? 'price' : 'min-pages',
				url = '/admin/Price/' + property + '/' + editId;

			$(el).change(function(e) {
				var value = this.value;

				$.post(url, { value: value }, function(res) {

				});
			});
		});
	}

	$b.on('click', 'a.delete-row-link', function(e) {
		if (! confirm('Вы уверены?')) {
			return false;
		}

		var $link = $(this);

		$.post(this.href, function(res) {
			if (res == 'ok') {
				var $tr = $link.parents('tr:eq(0)'),
					$table = $tr.parents('table:eq(0)');

				$tr.fadeOut(function() {
					$tr.remove();
				});
			}
		});

		return false;
	});

	var clsName = $('table.table').data('cls-name');

	var $tableDnD = $('table.table-dnd');

	if ($tableDnD.length) {
		$tableDnD.tableDnD({
			dragHandle: '.move',

			onDragClass: 'dragging',

			onDrop: function() {
				var ids = [];

				$tableDnD.find('tbody tr').each(function(i, tr) {
					var $tr = $(tr);

					ids.push($tr.data('record-id'));
				});

				$.post('/admin/' + clsName + '/change-position', {
					ids: ids.join(',')
				}, function(res) {
					if (! res.result) {
						alert('Error!');
					}
				});
			}
		});

	}

	var $sortable = $('ul.sortable');

	if ($sortable.length) {
		var $saveOrderButton = $('#saveOrderButton');

		$sortable.sortable({
			handle: '.move',

			stop: function() {
				var ids = [];

				$sortable.find('li').each(function(i, el) {
					var $el = $(el);

					ids.push($el.data('record-id'));
				});

				$.post('/admin/WorkPhoto/change-position', { ids: ids.join(',') }, function(res) {
					if (res.result) {
						$saveOrderButton.hide();
					}
				});
			}
		});

	}

	$b.on('click', 'a.delete-inline-link', function() {
		if (! confirm('Вы уверены?!')) {
			return false;
		}

		var $self = $(this);

		$.post(this.href, function() {
			$self.parents('li, tr').eq(0).fadeOut(function() {
				$(this).remove();
			});
		});

		return false;
	});
	var $moreFiles = $('#moreFiles');

	if ($moreFiles.length) {

		var $filesForm = $moreFiles.parents('form:eq(0)'),
			filesCount = 1;

		$moreFiles.click(function() {
			filesCount++;

			var tpl = [
					'<div class="controls">',
						'<input type="file" name="file' + filesCount + '" /><br/>',
						'<textarea class="span5" name="description' + filesCount + '" id="description' + filesCount + '" placeholder="Description"></textarea> ',
						'<input type="text" name="tags' + filesCount + '" placeholder="Tags" />',
					'</div>',
				].join(''),
				$newFileRow = $('<div />', {
					class: 'control-group',
					html: tpl
				});

			$moreFiles.before($newFileRow);

			return false;
		});

	}

	$('table.table input.editable-checkbox:checkbox').each(function(i, el) {
		var $el = $(el);

		$el.change(function() {
			clearTimeout($el.data('timer'));

			var data = {},
				$tr = $el.parents('tr:eq(0)'),
				recordId = $tr.data('record-id');
			data[el.name] = el.checked ? 1 : 0;

			$el.data('timer', setTimeout(function() {
				$.post('/admin/' + clsName + '/' + recordId + '?type=attr', data);
			}, 500));
		});
	});

	$('.editable-attr').each(function(i, el) {
		var $el = $(el);

		$el.dblclick(function() {
			if ($el.hasClass('editing')) {
				return false;
			}

			$el.addClass('editing');

			var oldHtml = $el.html(),
				type = $el.data('editable-type') || 'text',
				value = $el.data('editable-value') || $el.html().trim(),
				name = $el.data('editable-name'),
				truncate = $el.data('editable-truncate'),
				$tr = $el.parents('tr:eq(0)'),
				recordId = $tr.data('record-id');

			if (type == 'textarea') {
				$input = $('<textarea class="span4">' + value + '</textarea>');
			} else {
				$input = $('<input type="' + type + '" />');
				$input.val(value);
			}

			$el.html($input);

			$input[0].focus();

			$input.blur(function() {
				cancelEditing();
			});
			$input.change(function() {
				saveAttribute();
			});
			$input.keydown(function(e) {
				if (e.keyCode == 27 || e.keyCode == 9) {
					cancelEditing();
				}
				if (e.keyCode == 13 && e.ctrlKey) {
					saveAttribute();
				}
			});

			function cancelEditing() {
					$el.html(oldHtml);
					$el.removeClass('editing');
			}

			function saveAttribute() {
				var data = {}
				data[name] = $input.val();

				$.post('/admin/' + clsName + '/' + recordId + '?type=attr', data,function(res) {
					if (res.success) {
						var val = data[name]
						var val = truncate && truncate < val.length ? val.substr(0, truncate) + '...' : val;
						$el.html(val);
						$el.removeClass('editing')
					}
				});
			}

			return false;
		});
	});

});