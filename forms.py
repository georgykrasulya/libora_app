# coding: utf-8

from wtforms import (Form, SelectField, TextField, IntegerField, PasswordField,
    FileField, RadioField, BooleanField, TextAreaField,
    validators, widgets)

class LoginForm(Form):
    username = TextField(u'Логин', [validators.Required(u'Введите логин')])
    password = PasswordField(u'Пароль', [validators.Required(u'Введите пароль')])

PRODUCT_TYPES = (
    ("0", u'Кольца'),
    ("1", u'Серьги'),
    ("2", u'На шею'),
)

class ReviewForm(Form):
    name = TextField(u'Имя', [validators.Required()])
    text = TextField(u'Текст', widget=widgets.TextArea(), default='')
    photo_filename = FileField(u'Photo')
    place = TextField(u'Место', [validators.Required()])

class ProductForm(Form):
    title = TextField(u'Заголовок', [validators.Required(u'Введите заголовок')])
    description = TextField(u'Краткое описание', widget=widgets.TextArea(), default='')
    type_ = SelectField(u'Тип', choices=PRODUCT_TYPES)
    preview_filename = FileField(u'Превьюшка для списка')
    old_price = TextField(u'Старая цена', description=u'Для Sale. Только цифры')
    price = TextField(u'Цена', description=u'Только цифры')
    size = TextField(u'Размер', description=u'Размеры через запятую')
    stones = TextField(u'Камни')
    material = TextField(u'Материал')
    weight = TextField(u'Вес')
    sold = BooleanField(u'Продан')


class ProductPhotosForm(Form):
    photos = FileField(u'Фотографии внутри', description=u'Можно выбрать несколько фотографий')
