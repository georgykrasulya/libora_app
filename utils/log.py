import datetime
import logging
import os

from settings import LOG_DIR


now = datetime.datetime.now()

js_handler = logging.FileHandler(os.path.join(LOG_DIR, 'js', '{0}.log'.format(now.strftime('%Y-%m-%d'))))
js_logger = logging.getLogger('js_logger')
js_logger.addHandler(js_handler)

test_handler = logging.FileHandler(os.path.join(LOG_DIR, 'test', '{0}.log'.format(now.strftime('%Y-%m-%d'))))
test_logger = logging.getLogger('test_logger')
test_logger.addHandler(test_handler)

debug_handler = logging.FileHandler(os.path.join(LOG_DIR, 'debug', '{0}.log'.format(now.strftime('%Y-%m'))))
# debug_formatter = logging.Formatter(fmt='%(levelname)s, %(asctime)s, %(message)s in %(pathname)s at %(lineno)s')
debug_logger = logging.getLogger('debug_logger')
debug_logger.addHandler(debug_handler)
# debug_logger.setFormatter(debug_formatter)