import json
import re
import string
import random
import os
from PIL import Image
from functools import wraps
from cgi import escape

from flask import Response

from libora import app, mail

# class JSONMixin(object):
# 	def __json__(self):
		


rmobile_b = re.compile(r'iPad|(android|bb\\d+|meego).+mobile|avantgo|bada\\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino', re.I|re.M)
rmobile_v = re.compile(r'1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\\-(n|u)|c55\\/|capi|ccwa|cdm\\-|cell|chtm|cldc|cmd\\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\\-s|devi|dica|dmob|do(c|p)o|ds(12|\\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\\-|_)|g1 u|g560|gene|gf\\-5|g\\-mo|go(\\.w|od)|gr(ad|un)|haie|hcit|hd\\-(m|p|t)|hei\\-|hi(pt|ta)|hp( i|ip)|hs\\-c|ht(c(\\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\\-(20|go|ma)|i230|iac( |\\-|\\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\\/)|klon|kpt |kwc\\-|kyo(c|k)|le(no|xi)|lg( g|\\/(k|l|u)|50|54|\\-[a-w])|libw|lynx|m1\\-w|m3ga|m50\\/|ma(te|ui|xo)|mc(01|21|ca)|m\\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\\-2|po(ck|rt|se)|prox|psio|pt\\-g|qa\\-a|qc(07|12|21|32|60|\\-[2-7]|i\\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\\-|oo|p\\-)|sdk\\/|se(c(\\-|0|1)|47|mc|nd|ri)|sgh\\-|shar|sie(\\-|m)|sk\\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\\-|v\\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\\-|tdg\\-|tel(i|m)|tim\\-|t\\-mo|to(pl|sh)|ts(70|m\\-|m3|m5)|tx\\-9|up(\\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\\-|your|zeto|zte\\-', re.I|re.M)
rurl = re.compile(r'(((https?://)|(www\.))[^ ]+)')

def mail_send_silently(msg):
	if app.config['DEBUG']:
		# app.logger.info(msg.html)
		pass
	mail.send(msg)

def jsonable(f):
	@wraps(f)
	def wrapper(*args, **kwargs):
		response = f(*args, **kwargs)
		if isinstance(response, (dict, list, tuple)):
			return Response(json.dumps(response), mimetype='text/plain')
		return response
	return wrapper

def create_thumbnail(orig_path, path, size, crop=True, crop_percents=None, crop_coords=None):
	with open(orig_path) as f:
		img = Image.open(f)

		if not isinstance(size, (list, tuple)):
			size = size, size
		width, height = img.size

		app.logger.info('create_thumbnail(), crop_percents: {0}, will crop: {1}'.format(crop_percents, crop))

		if crop:
			if crop_percents is not None:
				left = int(width / 100.0 * crop_percents[0])
				upper = int(height / 100.0 * crop_percents[1])
				right = int(width / 100.0 * crop_percents[2])
				lower = int(height / 100.0 * crop_percents[3])

				s = '''
					orig_path: {orig_path}
					path: {path}
					cropping by percents, width: {width}, height: {height}
					crop(x, y, x2, y2): [{left}, {upper}, {right}, {lower}]
					crop_percents: {crop_percents}
				'''.format(**locals()).replace('\t', ' ')
				app.logger.info(s)
			elif crop_coords is not None:
				left, upper, right, lower = crop_coords

				s = 'cropping by coords, width: {width}, height: {height}\ncrop(x, y, x2, y2): [{left}, {upper}, {right}, {lower}]'.format(**locals())
				# app.logger.info(s)
			else:
				if width > height:
					delta = width - height
					left = int(delta/2)
					upper = 0
					right = height + left
					lower = height
				else:
					delta = height - width
					left = 0
					upper = int(delta/2)
					right = width
					lower = width + upper


			img = img.crop((left, upper, right, lower))


		img.thumbnail(size, Image.ANTIALIAS)
		img.save(path, quality=100)

def text2html(text):
	html = escape(text)
	html = html.replace('\n', '<br>')
	html = re.sub(rurl, r'<a target="_blank" href="\1">\1</a>', html)

	return html

def random_string(length):
	symbols = [s for s in (string.digits + string.letters)]
	s = []

	for x in xrange(length):
		s.append(random.choice(symbols))

	return ''.join(s)

def is_mobile(ua):
	b = rmobile_b.search(ua)
	v = rmobile_v.search(ua[0:4])
	return b or v

def touch(path):
    with file(path, 'a'):
        os.utime(path, None)