# encoding: utf-8

import time
import logging
import re

from libora import (db, app, product_preview_set, product_photos_set, review_photo_set)


class ModelMixin(object):
    def setattrs(self, **kwargs):
        for kw, val in kwargs.iteritems():
            if hasattr(self, kw):
                # logging.error('{}: {}, {}'.format(kw, val, type(val)))
                setattr(self, kw, val)
        if hasattr(self, 'date_added') and not self.date_added and 'date_added' not in kwargs:
            self.date_added = datetime.datetime.now()
        if hasattr(self, 'position') and not self.position and 'position' not in kwargs:
            self.position = int(time())


class Coupon(db.Model, ModelMixin):
    __tablename__ = 'coupons'

    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String(255), default='')
    discount = db.Column(db.Integer, default=10)


class Product(db.Model, ModelMixin):
    __tablename__ = 'products'
    __upload_set__ = product_preview_set

    RING = 0
    EARRINGS = 1
    NECK = 2

    TYPES = (
        (RING, u'Кольца'),
        (EARRINGS, u'Серьги'),
        (NECK, u'На шею'),
    )

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255))
    old_price = db.Column(db.Integer, default=0, index=True)
    price = db.Column(db.Integer)
    preview_filename = db.Column(db.String(255))
    photos = db.relationship('ProductPhoto', backref='project', lazy='dynamic')
    size = db.Column(db.String(100))
    stones = db.Column(db.String(100))
    material = db.Column(db.String(100))
    type_ = db.Column(db.Integer, default=0)
    weight = db.Column(db.Integer)
    sold = db.Column(db.Boolean, default=False)
    postponed = db.Column(db.Boolean, default=False)
    description = db.Column(db.Text)

    position = db.Column(db.Integer, index=True, default=lambda: int(time.time()))
    
    @classmethod
    def get_type_by_keyword(self, keyword):
        return {'ring': 0, 'earrings': 1, 'neck': 2}[keyword]

    def setattrs(self, **kwargs):
        super(Product, self).setattrs(**kwargs)

    def __repr__(self):
        return u'<Product %s, sold?: %s>' % (self.title, self.sold)

    def get_absolute_url(self):
        return u'/product/{}'.format(self.id)

    def get_type_display(self):
        return dict(self.TYPES)[self.type_]

    def get_size_display(self):
        size_list = self.get_size_list()
        if len(size_list) == 1:
            return size_list[0]
        return '{}-{}'.format(size_list[0], size_list[-1])

    def has_size(self):
        return self.type_ == self.RING

    @property
    def in_stock(self):
        return not (self.sold or self.postponed)
        
    def get_size_list(self):
        return list(sorted(s.strip() for s in self.size.split(',')))

    def get_preview_url(self):
        return product_preview_set.url(self.preview_filename)

    def get_price_as_int(self):
        if self.price:
            return int(float(re.sub(r'[^\d\.]+', '', str(self.price))))
        return 0

class ProductPhoto(db.Model, ModelMixin):
    __tablename__ = 'product_photos'
    __upload_set__ = product_photos_set

    id = db.Column(db.Integer, primary_key=True)
    photo_filename = db.Column(db.String(255))
    product_id = db.Column(db.Integer, db.ForeignKey('products.id'))

    def setattrs(self, **kwargs):
        super(Work, self).setattrs(**kwargs)

    def __repr__(self):
        return u'<Product %r>' % self.title

    def get_absolute_url(self):
        return u'/product/{}'.format(self.id)

    def get_photo_url(self):
        return self.__upload_set__.url(self.photo_filename)


class Review(db.Model, ModelMixin):
    __tablename__ = 'reviews'
    __upload_set__ = review_photo_set

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    place = db.Column(db.String(255))
    photo_filename = db.Column(db.String(255))
    text = db.Column(db.Text)

    def __repr__(self):
        return u'<Review %s>' % (self.id)

    def get_photo_url(self):
        return self.__upload_set__.url(self.photo_filename)